import actions from './actions'

const initState = {
  isLoading: false,
}

export default function reducer (
  state = initState,
  {type, payload}
) {

  switch (type) {
    case actions.LOAD_FROM_SERVER_USERS:
      return {
        ...initState,
        isLoading: true,
      }

    case actions.LOAD_FROM_SERVER_USERS_SUCCESS:
      return {
        ...state,
        ...payload,
        isLoading: false,
      }

    case actions.LOAD_FROM_SERVER_USERS_ERROR:
      return {
        ...state,
        ...payload,
        isLoading: false,
      }
    case actions.ADD_TO_SERVER_USER:
      return {
        ...state,
        isLoading: true,
      }

      case actions.ADD_TO_SERVER_USER_SUCCESS:
      const users = state.users || [];
      const { user } = payload;
      if (user) {
        users.push(user);
      }

      return {
        ...state,
        ...{users},
        isLoading: false,
      }

    case actions.ADD_TO_SERVER_USER_ERROR:
      return {
        ...state,
        ...payload,
        isLoading: false,
      }

    default:
      return state
  }
}
