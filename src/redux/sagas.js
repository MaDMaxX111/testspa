import {all} from 'redux-saga/effects';
import usersSagas from './Users/saga';

export default function* rootSaga(getState) {
    yield all([
        usersSagas(),
    ]);
}
