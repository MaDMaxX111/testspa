import React  from 'react'
import styled from 'styled-components'

import logo from '../../image/logo.svg'

const Loader = () => {

  const LoaderComponent = styled.div`
      min-height: 100%;
      width: 100%;
      position: fixed;
      top: 0;
      bottom: 0;
    
      &:after {
        content: '';
        display: block;
        position: absolute;
        top: 0;
        bottom: 0;
        width: 100%;
        background-image: url(${logo});
        background-repeat: no-repeat;
        background-position: center center;
        animation: loader 2s infinite linear;
      }
    
      @keyframes loader {
        0% {
          transform:rotate(0deg)
        }
        50%{
          transform:rotate(180deg)
        }
        100%{
          transform:rotate(360deg)
        }
      }
  `
  return (
    <LoaderComponent/>
  )
}

export default Loader