const DOCUMENT = 'USERS_'

const actions = {

  LOAD_FROM_SERVER_USERS: DOCUMENT + 'LOAD_FROM_SERVER_USERS',
  LOAD_FROM_SERVER_USERS_SUCCESS: DOCUMENT + 'LOAD_FROM_SERVER_USERS_SUCCESS',
  LOAD_FROM_SERVER_USERS_ERROR: DOCUMENT + 'LOAD_FROM_SERVER_USERS_ERROR',

  ADD_TO_SERVER_USER: DOCUMENT + 'ADD_TO_SERVER_USER',
  ADD_TO_SERVER_USER_SUCCESS: DOCUMENT + 'ADD_TO_SERVER_USER_SUCCESS',
  ADD_TO_SERVER_USER_ERROR: DOCUMENT + 'ADD_TO_SERVER_USER_ERROR',

  loadUsers: () => ({
    type: actions.LOAD_FROM_SERVER_USERS,
  }),
  loadUsersSuccess: (results) => ({
    type: actions.LOAD_FROM_SERVER_USERS_SUCCESS,
    payload: results,
  }),

  loadUsersError: (error) => ({
    type: actions.LOAD_FROM_SERVER_USERS_ERROR,
    payload: error,
  }),

  addUser: (data) => ({
    type: actions.ADD_TO_SERVER_USER,
    payload: data,
  }),

  addUserSuccess: (results) => ({
    type: actions.ADD_TO_SERVER_USER_SUCCESS,
    payload: results,
  }),

  addUserError: (error) => ({
    type: actions.ADD_TO_SERVER_USER_ERROR,
    payload: error,
  }),

}
export default actions
