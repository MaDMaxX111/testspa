import styled from 'styled-components';

const IndexWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export default IndexWrapper;
