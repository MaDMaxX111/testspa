import { all, takeEvery, put } from 'redux-saga/effects';
import actions from './actions';
import api from '../../api';
let lastId = 301;
function* loadFromServerUsers() {
    try {
        const results = yield api.getUsers();
        if (results) {
            yield put(actions.loadUsersSuccess({users: results}))
        }

    } catch (error) {
        yield put(actions.loadUsersError({error}));
    }
}

function* saveToServerUser({payload}) {
  try {
    const results = yield new Promise((resolve) => {
        setTimeout(()=>{resolve(lastId++)}, 2000)
    });

    if (results) {
      yield put(actions.addUserSuccess({user: Object.assign(payload, {id: results})}))
    }

  } catch (error) {
    yield put(actions.addUserError({error}));
  }
}

export default function* rootSaga() {
    yield all([
        takeEvery(actions.LOAD_FROM_SERVER_USERS, loadFromServerUsers),
        takeEvery(actions.ADD_TO_SERVER_USER, saveToServerUser),
    ]);
}
