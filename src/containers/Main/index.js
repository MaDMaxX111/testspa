import React, { Component } from 'react'
import { Route, Switch as Switcher } from 'react-router-dom'
import { Layout, Menu } from 'antd'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

import MainContainerWrapper from './index.style'
import { Routes, Menu as MenuItems } from '../../routes'
import Portal from '../../helpers/portal';

const {Header, Footer, Content} = Layout

class MainContainer extends Component {

  render () {
    const { pathname } = this.props
    return (
      <MainContainerWrapper>
        <Header>
          <Menu
            theme="dark"
            mode="horizontal"
            style={{lineHeight: '64px'}}
            defaultSelectedKeys={[pathname]}
          >
            {MenuItems.map((item, key) => <Menu.Item key={item.key}><NavLink to={item.key}/>{item.label}</Menu.Item>)}
          </Menu>
        </Header>
        <Content>
          <Switcher>
            {Routes.map((route, index) => (
              <Route
                key={new Date()}
                exact
                path={route.path}
                component={route.component}
              />
            ))}
          </Switcher>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          {`©${new Date().getFullYear()} Created by Me`}
        </Footer>
        <Portal />
      </MainContainerWrapper>
    )
  }
}

export default connect(state => {
    return {
      pathname: state.router ? state.router.location.pathname : null,
    }
  },
)(MainContainer)
