import { hot } from 'react-hot-loader/root'
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { store, history } from './redux/store';
import { ConnectedRouter } from 'connected-react-router'
import MainContainder from './containers/Main'
import 'antd/dist/antd.css';
class App extends Component {

  render() {
    return (
         <Provider store={store}>
            <ConnectedRouter history={history}>
              <MainContainder/>
            </ConnectedRouter>
        </Provider>
  )
  }
}

export default hot(App);
