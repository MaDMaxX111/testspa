import styled from 'styled-components';

const MainContainerWrapper = styled.div`
	display: flex;
	flex-direction: column;
	min-height: 100vh;
	overflow: hidden;

	main {
		flex-grow: 1;
		display: flex;
        padding: 0 50px;
        margin: 50px 0;
	}
`;

export default MainContainerWrapper;
