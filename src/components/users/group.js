import React from 'react'
import UserList from './list';

const UsersGroup = ({columns, data, isLoading, groupBy}) => {

    const groups = data ? data.reduce((acc, obj) => {
        let index = acc.findIndex((elm) => {
                return elm.some(e => e[groupBy] === obj[groupBy])
            }
        );
        if (index === -1) {
            return acc.concat([[Object.assign({}, obj)]])
        }
        ;
        acc[index].push(obj);
        return acc
    }, []) : [];
debugger
    return (
        <>
        {groups.map((group, index) => {
            const name = group[0][groupBy]
            return (
                <div key={index}>
                    <h2>{name ? `Сотрудники отдела '${name}'` : `Сотрудники вне отделов`}</h2>
                    <UserList data={group} columns={columns} isLoading={isLoading}/>
                </div>
            )
        })}
        </>
    )
}



export default UsersGroup
