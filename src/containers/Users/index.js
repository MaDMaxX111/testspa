import React, { Component } from 'react'
import UsersWrapper from './index.style'
import connect from 'react-redux/es/connect/connect'
import { Icon, Tabs, Button } from 'antd';
import actions from '../../redux/Users/actions'
import actionsPortal from '../../redux/Portal/actions'
import UserList from '../../components/users/list'
import UserGroup from '../../components/users/group'

const { loadUsers, addUser } = actions
const {
    showComponent,
    hideCompomnent,
} = actionsPortal;

const TabPane = Tabs.TabPane;

const columnsList = [
    {
        title: 'Имя',
        dataIndex: 'firstName',
        sorter: true,
        render: firstName => firstName,
        width: '33%',
    },
    {
        title: 'Фамилия',
        dataIndex: 'lastName',
        sorter: true,
        render: lastName => lastName,
        width: '33%',
    },
    {
        title: 'Отдел',
        dataIndex: 'department',
        sorter: false,
        render: department => department,
        width: '33%',
    },
];

const columnsGroup = [
    {
        title: 'Имя',
        dataIndex: 'firstName',
        sorter: true,
        render: firstName => firstName,
        width: '33%',
    },
    {
        title: 'Фамилия',
        dataIndex: 'lastName',
        sorter: true,
        render: lastName => lastName,
        width: '33%',
    },
];

class Users extends Component {
    componentDidMount() {
        this.props.loadUsers();
    }

    hanleShowModal = () => {
        this.props.showComponent({
            component: () => import('../../components/users/form'),
            props: {
                onCancel: this.props.hideCompomnent,
                onSave: (values) => {
                    this.props.addUser(values)
                    this.props.hideCompomnent()
                },
            }
        })
    }

    render() {
        const {users, isLoading} = this.props;

        const data = users ? users : [];

        return (
            <UsersWrapper>
                <div>
                    <Button type="primary" onClick={this.hanleShowModal}>Добавить пользователя</Button>
                </div>
                <Tabs defaultActiveKey="1">
                    <TabPane
                        tab={
                            <span>
                            <Icon type="apple"/>
                            Списком
                            </span>
                        }
                        key="1"
                    >
                        <UserList data={data} columns={columnsList} isLoading={ isLoading }/>
                    </TabPane>
                    <TabPane
                        tab={
                            <span>
                            <Icon type="android"/>
                                По отделам
                            </span>
                        }
                        key="2"
                    >
                        <UserGroup groupBy={'department'} data={data} columns={columnsGroup} isLoading={ isLoading }/>
                    </TabPane>
                </Tabs>
            </UsersWrapper>
        )
    }
}

export default connect(state => {
        return {
            ...state.Users
        }
    },
    {
        loadUsers,
        showComponent,
        hideCompomnent,
        addUser,
    }
)(Users)
