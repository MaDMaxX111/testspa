const getFieldOption = (fieldName, form) => {

  const { isFieldTouched } = form

  if (isFieldTouched(fieldName)) {
    return null
  }

  return {
    validateStatus: '',
    help: ''
  }
}

const hasErrors = (fieldsError) => {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

export { getFieldOption, hasErrors }
