import styled from 'styled-components';

const IndexWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-align: center;
`;

export default IndexWrapper;
