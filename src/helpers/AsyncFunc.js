import React, { Component } from 'react';
import Loader from '../components/utils/loader'
export default function asyncComponent(importComponent) {
  class AsyncFunc extends Component {
    state = {
      component: null,
    }
    componentWillUnmount() {
      this.mounted = false;
    }
    async componentDidMount() {
      this.mounted = true;
      const { default: Component } = await importComponent();
      if (this.mounted) {
        this.setState({
          component: <Component {...this.props} />,
        });
      }
    }

    render() {
      return this.state.component || <Loader/>
    }
  }
  return AsyncFunc;
}
