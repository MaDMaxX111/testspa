import Users from './Users/reducer';
import Portal from './Portal/reducer';

export default {
    Users,
    Portal,
};
