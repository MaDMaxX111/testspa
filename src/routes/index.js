import asyncComponent from '../helpers/AsyncFunc'

export const Routes = [
    {
        path: '/',
        component: asyncComponent(() =>
            import('../containers/Index')
        )
    },
    {
        path: '/users',
        component: asyncComponent(() =>
            import('../containers/Users')
        )
    },
    // {
    //     component: asyncComponent(() =>
    //         import("../containers/404")
    //     )
    // },
]

export const Menu = [
    {
        key: '/',
        label: 'Заглавная'
    },
    {
        key: '/users',
        label: 'Пользователи'
    },
]
