import React, { Component } from 'react'
import { Table } from "antd";
import sorting from "../../helpers/SortArray";
class UsersList extends Component {

    state = {
        sorter: null
    };

    handleTableChange = (pagination, filters, sorter) => {
        this.setState({sorter: sorter || null})
    };

    render() {
        const { sorter } = this.state;
        const { columns, data, isLoading } = this.props
        return(
            <Table
                columns={columns}
                rowKey={record => record.id}
                dataSource={sorter ? sorting(data, sorter) : data}
                pagination={{
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ['10', '20', '40', '80', '160', '300'],
                }}
                loading={isLoading}
                onChange={this.handleTableChange}
            />
        )
    }
}

export default UsersList
