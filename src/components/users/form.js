import React, { Component } from 'react'
import { Select, Form, Input, Modal } from 'antd'
import { getFieldOption, hasErrors } from '../../helpers/FormFunction'

const { Option } = Select
const FormItem = Form.Item;

class UserAddForm extends Component {

    componentDidMount() {
        this.props.form.validateFields((errors, values) => {});
    }

    handleCancel = () => {
        const {onCancel} = this.props
        onCancel && onCancel()
    }

    handleSubmit = e => {
        e.preventDefault();
        const { onSave } = this.props
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                onSave && onSave(values)
            }
        });
    };

    render() {
        const {form} = this.props;

        const {getFieldDecorator, getFieldsError} = form;
        return (
            <Modal
                visible={true}
                destroyOnClose={true}
                maskClosable={false}
                title={'Добавить сотрудника'}
                okText={'Добавить'}
                cancelText={'Отмена'}
                onCancel={this.handleCancel}
                onOk={this.handleSubmit}
                okButtonProps={{
                    disabled: hasErrors(getFieldsError())
                }}
            >
                <Form onSubmit={this.handleSubmit}>
                    <FormItem label="Имя" hasFeedback {...getFieldOption('firstName', form)} >
                        {getFieldDecorator('firstName', {
                          rules: [
                                {
                                    required: true,
                                    min: 3,
                                    message: 'Имя должно быть не меннее 3-х символов',
                                }
                            ],
                        })(<Input placeholder={'Имя'}/>)}
                    </FormItem>
                    <FormItem hasFeedback label="Фамилия" {...getFieldOption('lastName', form)}>
                        {getFieldDecorator('lastName', {
                            initialValue: '',
                          rules: [{
                            required: true,
                            min: 3,
                            message: 'Фамилия должно быть не меннее 3-х символов',
                          }],
                        })(<Input placeholder={'Имя'}/>)}
                    </FormItem>
                    <FormItem hasFeedback label="Отдел" {...getFieldOption('department', form)}>
                        {getFieldDecorator('department', {
                          initialValue: '',
                        })(
                            <Select>
                                <Option key={1} value="">нет</Option>
                                <Option key={2} value="Финансовый">Финансовый</Option>
                                <Option key={3} value="Юридический">Юридический</Option>
                                <Option key={4} value="Производственный">Производственный</Option>
                                <Option key={5} value="Технический">Технический</Option>
                            </Select>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        )
    }

}

const WrappedUserAddForm = Form.create({name: "user-add-form"})(UserAddForm)
export default WrappedUserAddForm
