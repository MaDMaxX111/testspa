// import _ from 'lodash'

const sortArray = (array, {columnKey, order}) => {
  if (!columnKey || !order) return array
  const startTime = performance.now();
  const sortedValues = array.map((e, index) => {return {index, value: e[columnKey].toLowerCase()}});
  sortedValues.sort((a, b) => {
    if (order === 'descend') {
      return a.value > b.value ? 1 : -1
    }
    return a.value < b.value ? 1 : -1
  })
  // const sortedArray = new SortArrayObject({array: _.clone(array), columnKey, order});
  const res = sortedValues.map(e => {
    return array[e.index]
  })
  const deltaTime = performance.now() - startTime
  console.warn('Сортировка: ', deltaTime)
  return res
}

// class SortArrayObject {
//   constructor ({array, columnKey, order}) {
//     this.columnKey = columnKey
//     this.order = order
//     return this.quickSort(array)
//   }
//
//   swap (items, firstIndex, secondIndex) {
//     const temp = items[firstIndex]
//     items[firstIndex] = items[secondIndex]
//     items[secondIndex] = temp
//   }
//
//   partition (items, left, right) {
//
//     const pivot = items[Math.floor((right + left) / 2)][this.columnKey]
//     let i = left,
//       j = right
//
//     while (i <= j) {
//
//       if (this.order === 'descend') {
//         while (items[i][this.columnKey] < pivot) {
//           i++
//         }
//         while (items[j][this.columnKey] > pivot) {
//           j--
//         }
//       } else {
//         while (items[i][this.columnKey] > pivot) {
//           i++
//         }
//         while (items[j][this.columnKey] < pivot) {
//           j--
//         }
//       }
//
//       if (i <= j) {
//         this.swap(items, i, j)
//         i++
//         j--
//       }
//     }
//     return i
//   }
//
//   quickSort (items, left, right) {
//
//     if (items.length > 1) {
//       left = left || 0
//       right = right || items.length - 1
//       const index = this.partition(items, left, right)
//       if (left < index - 1) {
//         this.quickSort(items, left, index - 1)
//       }
//       if (index < right) {
//         this.quickSort(items, index, right)
//       }
//     }
//     return items
//   }
// }
//
export default sortArray
